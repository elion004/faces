:homepage: https://arc42.org

:keywords: software-architecture, documentation, template, arc42

:numbered!:
**About Faces**

[role="lead"]
Faces is an interpreter based on Brainfuck

Created and maintained by Elion Bajrami

Template Revision: 8.0 EN (based on asciidoc), June 2022

(C)
We acknowledge that this document uses material from the arc 42 architecture template, https://arc42.org.
