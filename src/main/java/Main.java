import java.nio.ByteBuffer;
import java.util.*;

public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static int memPtr = 0;
    public static byte[] mem = new byte[255];
    public static ByteBuffer stdIn;
    public static ByteBuffer stdOut;
    public static ArrayList<String> src = new ArrayList<>();
    public static int srcPtr = 0;
    public static Stack<Integer> stack = new Stack<>();
    public static Integer bracketCount = 0;
    public static int[] lookUpTbl = new int[100];

//      >  :>
//      <  :<
//      +  :)
//      -  :(
//      .  :D
//      ,  :O
//      [  :[
//      ]  :]

    public static void main(String[] args) {
        System.out.println("You can use this signs:");
        System.out.println(":>\n" +
                            ":<\n" +
                            ":)\n" +
                            ":(\n" +
                            ":D\n" +
                            ":O\n" +
                            ":[\n" +
                            ":]");

        System.out.println("Lets go:=)");

        String input = scanner.nextLine();

        String[] array = input.split("(?<=\\G.{2})");

        src.addAll(Arrays.asList(array));

        manageBrackets();

        while (srcPtr <= src.size() - 1) {

            basicLogic();

            if (src.get(srcPtr).equals(":[")) {
                if (mem[memPtr] == 0) {
                    //set memPtr where bracket closes
                    while (!src.get(srcPtr).equals(":]")) {
                        srcPtr++;
                    }
                } else {
                    srcPtr++;
                    Continue();
                }
            }
            if (src.get(srcPtr).equals(":]")) {
                if (mem[memPtr] == 0) {
                    //set memPtr where bracket closes
                    srcPtr++;
                    continue;
                } else {
                    //Set into stack or something like that
                    srcPtr = lookUpTbl[srcPtr];
                    continue;
                }
            }

            srcPtr++;
        }
    }

    public static void Continue() {
        while (srcPtr <= src.size() - 1 && !src.get(srcPtr).equals(":]")) {
            basicLogic();

            if (src.get(srcPtr).equals(":[")) {
                if (mem[memPtr] == 0) {
                    //set memPtr where bracket closes
                    while (!src.get(srcPtr).equals(":]")) {
                        srcPtr++;
                    }
                } else {
                    srcPtr++;
                    Continue();
                    continue;
                }
            }

            srcPtr++;
        }
    }

    public static void manageBrackets() {
        for (int i = 0; i < src.size(); i++) {
            if (src.get(i).equals(":[")) {
                bracketCount++;
                stack.push(i);
            } else if (src.get(i).equals(":]")) {
                bracketCount--;
                lookUpTbl[i] = (int) stack.pop();
                lookUpTbl[lookUpTbl[i]] = i;
            }
        }
    }

    private static void basicLogic() {
        if (src.get(srcPtr).equals(":)")) {
            mem[memPtr]++;
        }

        if (src.get(srcPtr).equals(":(")) {
            mem[memPtr]--;
        }

        if (src.get(srcPtr).equals(":>")) {
            memPtr++;
        }

        if (src.get(srcPtr).equals(":<")) {
            memPtr--;
        }

        if (src.get(srcPtr).equals(":D")) {
            System.out.println(mem[memPtr]);
        }
    }
}
